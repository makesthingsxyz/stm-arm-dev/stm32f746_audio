#ifndef sample_h_INCLUDED
#define sample_h_INCLUDED

#include <stdint.h>

extern uint16_t _binary_data_Houston_wav_start;
extern uint32_t _binary_data_Houston_wav_size;

#define AUDIO_SAMPLE &_binary_data_Houston_wav_start
#define AUDIO_SAMPLE_SIZE (uint32_t)&_binary_data_Houston_wav_size

#endif // include/sample_h_INCLUDED

