/** main.cpp
 *
 * Billy Stevens
 *
 * Top level initialization and main application loop.
 *
 **/

#include <cstdio>
#include <cstdlib>

#include "bsp/stm32746g_discovery_audio.h"
#include "bsp/stm32746g_discovery_lcd.h"
#include "bsp/stm32746g_discovery_ts.h"
#include "sample.h"
#include "stm32f7xx_hal.h"

extern SAI_HandleTypeDef haudio_out_sai;

extern "C" {
void DMA2_Stream4_IRQHandler(void) { HAL_DMA_IRQHandler(haudio_out_sai.hdmatx); }
}

int main() {
    // Initialize LED GPIO.
    BSP_LED_Init(LED1);

    // Initialize LCD and framebuffer.
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(0, LCD_FB_START_ADDRESS);
    BSP_LCD_Clear(LCD_COLOR_BLACK);
    BSP_LCD_SetBackColor(LCD_COLOR_BLACK);
    BSP_LCD_SetFont(&Font12);
    BSP_LCD_DisplayOn();

    // Initialize touch screen.
    if (BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize()) == TS_OK) {
        fprintf(stderr, "Touchscreen connected.\n");
    } else {
        fprintf(stderr, "Touchscreen not responding.\n");
    }

    // Initialize audio.
    if (BSP_AUDIO_OUT_Init(OUTPUT_DEVICE_HEADPHONE, 60, 8000) == AUDIO_OK) {
        fprintf(stderr, "Audio system initialized.\n");
        BSP_AUDIO_OUT_Play(AUDIO_SAMPLE, AUDIO_SAMPLE_SIZE);
        fprintf(stderr, "Audio sent.\n");
    } else {
        fprintf(stderr, "Audio system not responding.\n");
    }

    TS_StateTypeDef ts_state;
    while (1) {
        if (BSP_TS_GetState(&ts_state) != TS_OK) {
            fprintf(stderr, "Touchscreen read failed.\n");
        }

        if (ts_state.touchDetected != 0) {
            fprintf(stdout, "Position %d, %d.\x1B[K\n\x1BM", ts_state.touchX[0], ts_state.touchY[0]);
            fflush(stdout);
        }
    }
    // Infinite loop, never return.
}
