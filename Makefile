PREFIX = arm-none-eabi-
GDB = $(PREFIX)gdb
CXX = $(PREFIX)g++
CC  = $(PREFIX)gcc
LD  = $(PREFIX)gcc
OBJDUMP = $(PREFIX)objdump
OBJCOPY = $(PREFIX)objcopy

SRC_DIRS = system/ src/
OBJS  += $(patsubst %,build/%.o,$(shell find $(SRC_DIRS) -name '*.c' -or -name '*.cpp'))
OBJS  += $(patsubst %,build/%.o,$(shell find data/ -type f))
DEPS   = $(OBJS:%.o=%.d)

DEFS		+= -DSTM32F746xx
DEFS		+= -DUSE_HAL_DRIVER

INCS		+= -Iinclude
INCS		+= -Isystem/include
INCS		+= -Isystem/include/stm32f7-hal
INCS		+= -Isystem/include/cmsis
INCS		+= -Isystem/include/arm
INCS		+= -Isystem/include/components
INCS		+= -Isystem/inclue/cortexm
INCS		+= -Isystem/include/bsp

ARCH_FLAGS   = -mthumb -mcpu=cortex-m7 -mfpu=fpv5-sp-d16

CXXFLAGS    += -std=gnu++11
CFLAGS      += -std=gnu11
CPPFLAGS	+= -ggdb3 -Wall -Wpedantic -MD $(DEFS) $(INCS) $(ARCH_FLAGS)

LDSCRIPT 	 = system/stm32f746-disco.ld

LDFLAGS     += -static -nostartfiles
LDFLAGS		+= $(ARCH_FLAGS)

LDLIBS	 	+= -Wl,-lc -Wl,-lgcc -Wl,-lnosys

MAKEFLAGS += --no-print-directory

.SILENT:
.SECONDARY:
.PHONY: clean all flash

all: build/firmware.elf build/firmware.bin

%.bin: %.elf
	printf "  OBJCOPY $(*).bin\n"
	$(OBJCOPY) -Obinary $(*).elf $(*).bin

%.list: %.elf
	printf "  OBJDUMP $(*).list\n"
	$(OBJDUMP) -S $(*).elf > $(*).list

build/firmware.elf: $(OBJS) $(LDSCRIPT)
	printf "  LD      $(@)\n"
	$(LD) $(LDFLAGS) -T$(LDSCRIPT) $(OBJS) $(LDLIBS) -o $@

build/%.c.o: %.c
	@mkdir -p $(@D)
	printf "  CC      $(*).c\n"
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

build/%.cpp.o: %.cpp
	@mkdir -p $(@D)
	printf "  CXX     $(*).cpp\n"
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) -o $@ -c $<

build/%.o: %
	@mkdir -p $(@D)
	printf "  DAT     $(*)\n"
	$(PREFIX)ld -r -b binary -o $@ $<

flash: build/firmware.bin
	$(GDB) -ex "set confirm off" -ex "load" -ex "monitor reset run" -ex "quit"

clean:
	printf "  CLEAN\n"
	$(RM) -r build/

-include $(DEPS)
